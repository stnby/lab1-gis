var map = L.map('map').setView([51.505, -0.09], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([54.904897, 23.956548]).addTo(map)
    .bindPopup('<b>KTU Technical Center</b><br>LT-51367 Kaunas, Studentų 48a-101<br>Phone: +370 37 300 645, +370 37 300640<br>Fax: + 370 37 300 643<br>info@litnet.lt.')
    .openPopup();

L.marker([55.723845, 21.124598]).addTo(map)
    .bindPopup('<b>KU Technical Center</b><br>Klaipėda University<br>Herkaus Manto 84, LT - 92294 Klaipėda<br><br>Rolandas Garška<br>Chief Engineer of Information  Systems and Technology Centre at Klaipėda University<br>rolandas.garska@ku.lt,<br>phone  +370 46  39 88 88, fax +370 46  39 88 82');

L.marker([55.928657, 23.314546]).addTo(map)
    .bindPopup('<b>SU Technical center</b>><br>Šiauliai university<br>P.Višinskio 25, LT-76351  Šiauliai<br><br>Mindaugas Stoncelis<br>Institute of Informatics, Mathematics and E-Studies at Šiauliai university<br>mindaugas.stoncelis@su.lt,<br>phone +370 41 59 57 37');

L.marker([54.899138, 23.914789]).addTo(map)
    .bindPopup('<b>VDU Technical center</b><br><br>Vytautas Magnus university<br>K. Donelaičio g. 58, LT-44248 Kaunas<br><br>Darius Jonaitis<br><br>egmantas.girnis@vdu.lt,<br>+370 37 32 79 11');

L.marker([54.722514, 25.337704]).addTo(map)
    .bindPopup('<b>VGTU Technical Center</b><br>Vilnius Gediminas technical university<br>Saulėtekio al. 11, LT-10223 Vilnius<br>Remigijus K<br>Deputy of Head of Centre of information technology and systems at  Vilnius Gediminas technical univer<br>el.p. remigijus.kutas@vgtu.lt, +370 5 2745020, mob. +370 686 12567');

L.marker([54.682682, 25.286764]).addTo(map)
    .bindPopup('<b>VU Technical Center</b><br>Vilnius University<br>Universiteto g. 3, LT-01513 Vilnius<br><br>Algimantas Kutka<br>Expert for Networking at Vilnius University NOC<br>algimantas.kutka@itpc.vu.lt, phone +370 5 2366203<br><br>More: www.litnet.tinklas.vu.lt');
